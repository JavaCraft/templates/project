package org.javacraft.project;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Plugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getLogger().info("JavaCraft plugin enabled.");

        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        getLogger().info("JavaCraft plugin disabled.");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        String playerName = player.getPlayerProfile().getName();

        String welcomeMessage = String.format("Welcome %s! This message was sent from JavaCraft plugin.", playerName);

        player.sendMessage(welcomeMessage);
    }
}
